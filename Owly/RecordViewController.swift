//
//  RecordViewController.swift
//  Owly
//
//  Created by Mohamad Kamal Zakaria on 06/10/2018.
//  Copyright © 2018 Nikz Tech. All rights reserved.
//

import UIKit
import Speech
import SwiftGifOrigin
//import SwiftSiriWaveformView

protocol RecordDelegate {
    func confirmed(transactions: [Transaction])
}

class RecordViewController: UIViewController {
    
    var delegate: RecordDelegate?
    var selectedLang = 0
    
    // constants
    let spending = ["buy", "purchase", "pay", "spend", "watch", "eat", "drink", "fill", "spent"]
    
    let earning = ["receive", "earn", "gain", "get", "got"]
    
    let foodAndDrink = ["bữa trưa", "bữa sáng", "món ăn", "thực phẩm", "Maggie", "snack", "snacks", "breakfast", "lunch", "dinner", "sandwich", "pizza", "coke", "water", "teh tarik", "nasi lemak", "snickers", "Pringle", "Pringles"]
    
    let travel = ["xăng xe", "xăng dầu", "du lịch", "fuel", "gas", "petrol", "patrol", "Petronas","Petron", "shell", "bhp", "Caltex", "parking", "air asia", "Malindo", "Malaysia Airlines", "MAS", "flight", "toll", "tolls", "holiday"]
    
    let bill = ["của tôi", "hóa đơn", "electricity", "electric", "Internet", "unify", "Maxes", "water bill", "Indah Water", "house rental", "TNB", "top-up", "top up", "Celcom", "Digi", "UMobile", "Telekom", "telephone"]
    
    let entertainment = ["phim", "tạp chí", "sự giải trí", "giải trí", "magazine", "novel", "cinema", "movies", "movie", "concert", "TGV", "GSC", "MBO", "comic", "gym"]
    
    let clothing = ["quần jean", "đôi giày", "đồng hồ", "quần áo", "may mặc", "pants", "shirt", "t-shirt", "jacket", "hoodies", "hoodie", "watches", "watch", "bracelet","skirt", "tudung", "hijab","shoes", "sneakers", "socks", "fashion valet", "underwear", "brief", "shorts", "short", "belt", "jeans", "tie", "neck tie", "glasses", "shades", "sunglasses", "bra", "handbag"]
    
    let script = "today i spent 6 80 on breakfast, 8 90 for lunch, paid 69 ringgit for car petrol, 9 90 on magazine, then 119 ringgit for my internet bill, 16 for movie, and 64 ringgit for jeans"
    
    let numbers = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
    
    
    // variables
    var isBuy = true
    var newTransactions = [Transaction]()
    var selectedDate: Date!
    var lastDetectedWord = ""
    var last2DetectedWord = ""
    
    // variable views
    var tableView = UITableView()
    let recognizedTextView = UITextView()
    let confirmButton = RoundedButton()
    let confirmationLabel = UILabel()
    
    
    // text recognition stuff
    var audioEngine: AVAudioEngine!
    var speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "vi"))
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        recordAndRecognizeSpeech()
    }
}


extension RecordViewController {
    func setupViews() {
        
        view.backgroundColor = UIColor.white
        
        let owlyImageView = UIImageView()
        owlyImageView.image = UIImage.gif(name: "owly-kepak2")
        owlyImageView.contentMode = .scaleAspectFit
        owlyImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        let titleLabel = UILabel()
        titleLabel.text = "Tell me your spending today"
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.systemFont(ofSize: 36, weight: .medium)
        titleLabel.textColor = UIColor.white
        
        let titleStack = UIStackView(arrangedSubviews: [owlyImageView, titleLabel])
        titleStack.setup(axis: .horizontal, distribution: .fill, alignment: .fill, spacing: 20)
        titleStack.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        let headerView = UIImageView()
        headerView.image = UIImage.init(named: "background")
        headerView.translatesAutoresizingMaskIntoConstraints = false
        
        let recordingImage = UIImageView()
        recordingImage.contentMode = .scaleAspectFit
        recordingImage.image = UIImage.gif(name: "googleWave")
        recordingImage.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "transactions")
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        
        recognizedTextView.font = UIFont.systemFont(ofSize: 13)
        recognizedTextView.textColor = UIColor.white
        recognizedTextView.isScrollEnabled = false
        recognizedTextView.isEditable = false
        recognizedTextView.backgroundColor = UIColor(white: 0.0, alpha: 0.3)
        recognizedTextView.clipsToBounds = true
        recognizedTextView.layer.cornerRadius = 5
        
        confirmationLabel.text = "Is the information above correct?"
        confirmationLabel.textAlignment = .center
        confirmationLabel.isHidden = true
        
        let cancelButton = RoundedButton()
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelTapped), for: .touchUpInside)
        
        confirmButton.setTitle("Confirm", for: .normal)
        confirmButton.addTarget(self, action: #selector(confirmTapped), for: .touchUpInside)
        confirmButton.isHidden = true
        
        let buttonStack = UIStackView(arrangedSubviews: [cancelButton, confirmButton])
        buttonStack.setup(axis: .horizontal, distribution: .fillEqually, alignment: .fill, spacing: 20)
        buttonStack.setContentHuggingPriority(.required, for: .vertical)
        
        let stackView = UIStackView(arrangedSubviews: [titleStack, recordingImage, tableView, recognizedTextView, confirmationLabel, buttonStack])
        stackView.setup(axis: .vertical, distribution: .fill, alignment: .fill, spacing: 10)
        
        view.addSubview(headerView)
        view.addSubview(stackView)
        
        stackView.inflate(view: view, margin: 20)
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            ])
    }
}


// ===== BUTTON FUNCTIONS =====

extension RecordViewController {
    @objc func cancelTapped() {
        stopRecording()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func confirmTapped() {
        stopRecording()
        self.dismiss(animated: true, completion: nil)
        delegate?.confirmed(transactions: newTransactions)
    }
}


// ===== TABLEVIEW FUNCTIONS =====

extension RecordViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactions", for: indexPath)
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.numberOfLines = 0
        let transaction = newTransactions[indexPath.row]
        cell.textLabel?.text = "\(transaction.name ?? "?") (\(transaction.category ?? "?")) - \(transaction.amount?.localCurrenncy(locale: selectedLang == 0 ? Locale.init(identifier: "en") : Locale.init(identifier: "vi")) ?? "?")"
        return cell
    }
}



// ===== Speech Functions =====

extension RecordViewController {
    
    func recordAndRecognizeSpeech() {
        if selectedLang == 0 {
            speechRecognizer = SFSpeechRecognizer()
        } else {
            speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "vi"))
        }
        audioEngine = AVAudioEngine()
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, _) in
            self.request.append(buffer)
        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            return
        }
        
        guard let myRecognizer = SFSpeechRecognizer() else { return }
        if !myRecognizer.isAvailable { return }
        
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { (result, error) in
            if let error = error { print(error.localizedDescription); return }
            if let result = result {
                let bestString = result.bestTranscription.formattedString
                self.recognizedTextView.text = bestString
                
                var transaction = Transaction()
                
                for segment in result.bestTranscription.segments {
                    let detectedWord = segment.substring
//                    print(detectedWord)
                    
//                    let date = Date()
                    transaction.date = self.selectedDate.string(format: "d/M/yy")
                    
                    // check if its earning, else automatic buy
                    if self.earning.contains(detectedWord) { self.isBuy = false }
                    
                    // check for amount
                    if detectedWord.isNumeric {
                        print(detectedWord)
                        var amount = detectedWord.float
                        if self.selectedLang == 0 {
                        if amount.truncatingRemainder(dividingBy: 100) != 0 && amount > 100 { amount = amount/100 }
                        } else {
                            amount *= 1000
                        }
                        print(amount)
                        transaction.amount = amount
                    }
                    
                    if detectedWord == "ringgit" && self.selectedLang == 0 {
                        var amount = transaction.amount ?? 100
                        if amount < 100 && amount.truncatingRemainder(dividingBy: 1) != 0 { amount = amount * 100 }
                        transaction.amount = amount
                    }
                    
                    if self.numbers.contains(detectedWord) && self.selectedLang == 0 {
                        switch detectedWord {
                        case "one", "một":
                            transaction.amount = 1
                        case "two", "hai":
                            transaction.amount = 2
                        case "three", "ba":
                            transaction.amount = 3
                        case "four", "bốn":
                            transaction.amount = 4
                        case "five", "năm":
                            transaction.amount = 5
                        case "six", "sáu":
                            transaction.amount = 6
                        case "seven", "bảy":
                            transaction.amount = 7
                        case "eight", "tám":
                            transaction.amount = 8
                        case "nine", "chín":
                            transaction.amount = 9
                        default:
                            transaction.amount = 0
                        }
                    }
                    
                    // check for item
                    if self.foodAndDrink.contains(detectedWord) {
                        transaction.name = detectedWord.capitalized
                        transaction.category = "Food & Drinks"
                    } else if self.travel.contains(detectedWord) {
                        transaction.name = detectedWord.capitalized
                        transaction.category = "Travel"
                    } else if self.bill.contains(detectedWord) {
                        transaction.name = detectedWord.capitalized
                        transaction.category = "Bill"
                    } else if self.entertainment.contains(detectedWord) {
                        transaction.name = detectedWord.capitalized
                        transaction.category = "Entertainment"
                    } else if self.clothing.contains(detectedWord) {
                        transaction.name = detectedWord.capitalized
                        transaction.category = "Apparel"
                    }
                    
                    // 2 words
                    let last2Words = self.lastDetectedWord + " " + detectedWord
                    if self.foodAndDrink.contains(last2Words) {
                        transaction.name = last2Words.capitalized
                        transaction.category = "Food & Drinks"
                    } else if self.travel.contains(last2Words) {
                        transaction.name = last2Words.capitalized
                        transaction.category = "Travel"
                    } else if self.bill.contains(last2Words) {
                        transaction.name = last2Words.capitalized
                        transaction.category = "Bill"
                    } else if self.entertainment.contains(last2Words) {
                        transaction.name = last2Words.capitalized
                        transaction.category = "Entertainment"
                    } else if self.clothing.contains(last2Words) {
                        transaction.name = last2Words.capitalized
                        transaction.category = "Apparel"
                    }
                    
                    if self.last2DetectedWord != self.lastDetectedWord { self.last2DetectedWord = self.lastDetectedWord }
                    if self.lastDetectedWord != detectedWord { self.lastDetectedWord = detectedWord }
                    
                    print(self.lastDetectedWord + " " + detectedWord)
                    print(self.last2DetectedWord + " " + self.lastDetectedWord + " " + detectedWord)
                    
                    if transaction.amount != nil, transaction.category != nil, transaction.date != nil, transaction.name != nil {
                        self.confirmationLabel.isHidden = false
                        self.confirmButton.isHidden = false
                        print(transaction)
                        var previousTransactions = [String]()
                        self.newTransactions.forEach({
                            previousTransactions.append($0.name!)
                        })
                        if self.newTransactions.last == nil || !previousTransactions.contains(transaction.name!) {
                            self.newTransactions.append(transaction)
                            self.tableView.reloadData()
                        }
                        transaction = Transaction()
                    }
                }
            }
        })
    }
    
    
    fileprivate func stopRecording() {
        audioEngine.stop()
        request.endAudio()
        recognitionTask?.cancel()
    }
}






class RoundedButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//        backgroundColor = UIColor.etiqaYellow
        layer.cornerRadius = 17
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
        setTitleColor(UIColor.black, for: .normal)
        clipsToBounds = true
    }
    
    func text(text: String) {
        let attributedString = NSAttributedString(string: text, attributes: [.font: UIFont.systemFont(ofSize: 15), .foregroundColor: UIColor.white])
        setAttributedTitle(attributedString, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
