//
//  CustomViews.swift
//  Owly
//
//  Created by Mohamad Kamal Zakaria on 06/10/2018.
//  Copyright © 2018 Nikz Tech. All rights reserved.
//

import UIKit
import FSCalendar

class Inset10Label: UILabel {
    
    let topInset: CGFloat = 5
    let leftInset: CGFloat = 10
    let bottomInset: CGFloat = 5
    let rightInset: CGFloat = 10
    
    convenience init(text: String) {
        self.init()
        self.text = text
        textAlignment = .center
        font = UIFont.systemFont(ofSize: 17)
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}



class MyLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textAlignment = .center
        textColor = UIColor.red
        font = UIFont.systemFont(ofSize: 15)
        numberOfLines = 0
    }
    
    convenience init(text: String) {
        self.init()
        self.text = text
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class CustomTextField: UITableViewCell {
    
    let inputField = UITextField()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let rmLabel = UILabel()
        rmLabel.text = "RM"
        rmLabel.textColor = UIColor.darkGray
        rmLabel.font = UIFont.systemFont(ofSize: 19, weight: .medium)
        rmLabel.setContentHuggingPriority(.required, for: .horizontal)
        
        inputField.keyboardType = .numberPad
        inputField.textColor = UIColor.darkGray
        inputField.font = UIFont.systemFont(ofSize: 19, weight: .medium)
        
        let inputStack = UIStackView(arrangedSubviews: [rmLabel, inputField])
        inputStack.setup(axis: .horizontal, distribution: .fill, alignment: .fill, spacing: 10)
        
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        
        addSubview(inputStack)
        addSubview(line)
        
        inputStack.inflate(view: self, margin: 0)
        line.setConstraint(view: self, top: nil, leading: 10, bottom: -5, trailing: 10, width: nil, height: 1, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
