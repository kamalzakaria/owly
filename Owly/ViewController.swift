//
//  ViewController.swift
//  Owly
//
//  Created by Mohamad Kamal Zakaria on 06/10/2018.
//  Copyright © 2018 Nikz Tech. All rights reserved.
//

import UIKit
import Speech
import FSCalendar

// Model
struct Transaction {
    var name: String?
    var amount: Float?
    var date: String?
    var category: String?
}


// Controller

class ViewController: UIViewController {
    
    // constants
    let headers = ["owly", "calendar", "expenses", "bottom"]
    
    // variables
    var groupedTransactions = [[Transaction]]()
    var transactions = [Transaction]()
    var todaysSpending: Float?
    var selectedDate: Date?
    var settingButton = UIButton(type: .custom)
    var dailyBudget: Float = 100
    var income: Float = 3000
    var houseLoan: Float = 0
    var carLoan: Float = 0
    var personalLoan: Float = 0
    var selectedLang = 0
    
    // variable views
    let tableView = UITableView()
    var langButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
         // Do any additional setup after loading the view, typically from a nib.
        setupDummyData()
        setupViews()
    }


    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}


// ===== MISC FUNCTIONS ======

extension ViewController {
    func setupViews() {
        
        view.backgroundColor = UIColor.white
        
        let backgroundView = UIImageView()
        backgroundView.contentMode = .scaleAspectFill
        backgroundView.image = #imageLiteral(resourceName: "background")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        settingButton.setImage(#imageLiteral(resourceName: "settings-work-tool"), for: .normal)
        settingButton.addTarget(self, action: #selector(settingsTapped), for: .touchUpInside)
        
        let recordButton = UIButton(type: .custom)
        recordButton.setImage(#imageLiteral(resourceName: "mic"), for: .normal)
        recordButton.backgroundColor = UIColor.red
        recordButton.clipsToBounds = true
        recordButton.layer.cornerRadius = 25
        recordButton.addTarget(self, action: #selector(recordTapped), for: .touchUpInside)
        
        let stackView = UIStackView(arrangedSubviews: [tableView])
        stackView.setup(axis: .vertical, distribution: .fill, alignment: .fill, spacing: 0)
        
        view.addSubview(backgroundView)
        view.addSubview(stackView)
        view.addSubview(recordButton)
        view.addSubview(settingButton)
        
        stackView.setConstraint(view: view, top: -20, leading: 0, bottom: 20, trailing: 0, width: nil, height: nil, x: nil, y: nil, safeAreaLayout: false)
        backgroundView.setConstraint(view: view, top: 0, leading: 0, bottom: nil, trailing: 0, width: nil, height: nil, x: nil, y: nil, safeAreaLayout: false)
        recordButton.setConstraint(view: view, top: nil, leading: nil, bottom: 10, trailing: nil, width: 50, height: 50, x: 0, y: nil)
        settingButton.setConstraint(view: view, top: 10, leading: nil, bottom: nil, trailing: 10, width: nil, height: nil, x: nil, y: nil)
        
        langButton = UIButton()
        langButton.addTarget(self, action: #selector(langTapped), for: .touchUpInside)
        langButton.setTitle("EN", for: .normal)
        view.addSubview(langButton)
        langButton.setConstraint(view: view, top: 0, leading: 0, bottom: nil, trailing: nil, width: nil, height: nil, x: nil, y: nil)
    }
    
    func setupDummyData() {
        transactions = [Transaction(name: "Sandwich", amount: 5.50, date: "6/10/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 15.90, date: "6/10/18", category: "Food & Drinks"),
                        Transaction(name: "Snacks", amount: 3.40, date: "6/10/18", category: "Food & Drinks"),
                        Transaction(name: "T-Shirt", amount: 9.90, date: "6/10/18", category: "Apparel"),
                        Transaction(name: "Sandals", amount: 19.90, date: "6/10/18", category: "Apparel"),
                        Transaction(name: "Lightning Cable", amount: 9.90, date: "6/10/18", category: "Entertainment"),
                        Transaction(name: "Parking Ticket", amount: 10.00, date: "6/10/18", category: "Travel"),
                        Transaction(name: "Tolls", amount: 12.40, date: "6/10/18", category: "Travel"),
                        Transaction(name: "Petrol", amount: 50.50, date: "6/10/18", category: "Travel"),
                        Transaction(name: "Glasses", amount: 190.90, date: "6/10/18", category: "Apparel"),
                        Transaction(name: "Pen", amount: 8.20, date: "6/10/18", category: "Entertainment"),
                        Transaction(name: "Topup", amount: 10.60, date: "6/10/18", category: "Bill"),
                        Transaction(name: "Mineral Water", amount: 2.50, date: "6/10/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 5.50, date: "5/10/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 40.80, date: "4/10/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 12.40, date: "3/10/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 8.20, date: "2/10/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 14.90, date: "1/10/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 55.50, date: "5/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 20.80, date: "4/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 82.40, date: "3/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 8.20, date: "2/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 1.90, date: "1/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 95.50, date: "6/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 140.80, date: "7/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 12.40, date: "8/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 38.20, date: "9/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 14.90, date: "10/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 65.50, date: "11/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 40.80, date: "12/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 12.40, date: "13/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 78.20, date: "14/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 14.90, date: "15/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 85.50, date: "16/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 40.80, date: "17/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 12.40, date: "18/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 98.20, date: "19/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 14.90, date: "20/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 5.50, date: "21/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 40.80, date: "22/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 12.40, date: "23/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 118.20, date: "24/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 14.90, date: "25/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 125.50, date: "26/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 40.80, date: "27/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 162.40, date: "28/9/18", category: "Food & Drinks"),
                        Transaction(name: "Lunch", amount: 8.20, date: "29/9/18", category: "Food & Drinks"),
]
        
        let date = Date()
        updateTransactionsData(date: date)
        selectedDate = date
    }
    
    func updateTransactionsData(date: Date) {
        let dateStr = date.string(format: "d/M/yy")
        var todaysTransactions = transactions.filter({ $0.date == dateStr })
        todaysTransactions = todaysTransactions.sorted(by: { $0.amount! > $1.amount! })
        todaysSpending =  todaysTransactions.reduce(0) { $0 + $1.amount! }
        groupedTransactions = todaysTransactions.group(by: { $0.category })
    }
}


// ===== BUTTON FUNCTIONS ======

extension ViewController {
    @objc func settingsTapped() {
        let vc = SettingsViewController()
        vc.delegate = self
        vc.income = income
        vc.houseLoan = houseLoan
        vc.carLoan = carLoan
        vc.personalLoan = personalLoan
        present(vc, animated: true, completion: nil)
    }
    
    @objc func recordTapped() {
        
        let vc = RecordViewController()
        vc.selectedDate = selectedDate
        vc.selectedLang = self.selectedLang
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @objc func langTapped() {
        selectedLang = selectedLang == 0 ? 1 : 0
        langButton.setTitle(selectedLang == 0 ? "EN" : "VI", for: .normal)
    }
}


// ===== DELEGATES & DATASOURCES =====

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headers.count + groupedTransactions.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section >= headers.count - 1 {
            let headerView = TransactionHeaderView()
            headerView.title.text = groupedTransactions[section - headers.count + 1][0].category

            let thisCategorysTransactions = groupedTransactions[section - headers.count + 1]
            let sum = thisCategorysTransactions.reduce(0) { $0 + $1.amount! }
            headerView.totalTransaction.text = sum.currency
            return headerView
        } else {
            let view = UIView()
            view.backgroundColor = UIColor.clear
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let lastSection = groupedTransactions.count + headers.count - 1
        switch section {
        case 0,1,2, lastSection:
            return 0
        default:
            return 35
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let lastSection = groupedTransactions.count + headers.count - 1
        switch section {
        case 0,1,2, lastSection:
            return 1
        default:
            return groupedTransactions[section - headers.count + 1].count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let lastSection = groupedTransactions.count + headers.count - 1
        switch indexPath.section {
        case 0:
            let cell = SummaryCell()
            return cell
        case 1:
            let cell = CalendarTableViewCell()
            cell.calendarView.delegate = self
            cell.calendarView.dataSource = self
            cell.calendarView.select(selectedDate ?? Date())
            return cell
        case 2:
            if groupedTransactions.count == 0 {
                let cell = UITableViewCell()
                cell.backgroundColor = UIColor.clear
                cell.textLabel?.text = "No Transaction Today~"
                cell.textLabel?.textAlignment = .center
                return cell
            } else {
                let cell = ExpenseCell()
                cell.selectedDate = self.selectedDate
                cell.todaysSpending = self.todaysSpending
                return cell
            }
        case lastSection:
            if groupedTransactions.count == 0 {
                let cell = UITableViewCell()
                cell.backgroundColor = UIColor.clear
                return cell
            } else {
                let cell = BottomCell()
                return cell
            }
        default:
            let cell = TransactionCell()
            cell.transaction = groupedTransactions[indexPath.section - headers.count + 1][indexPath.row]
            return cell
        }
    }
}


extension ViewController: SettingsDelegate {
    func didSave(budget: Float, income: Float, houseLoan: Float, carLoan: Float, personalLoan: Float) {
        self.income = income
        self.houseLoan = houseLoan
        self.carLoan = carLoan
        self.personalLoan = personalLoan
        dailyBudget = budget
        tableView.reloadData()
    }
}

extension ViewController: RecordDelegate {
    func confirmed(transactions: [Transaction]) {
        transactions.forEach({
            self.transactions.append($0)
        })
        updateTransactionsData(date: selectedDate!)
        tableView.reloadData()
    }
}


// ===== Calendar Properties

extension ViewController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        return UIColor.black
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let today = Date()
        let datesTransactions = transactions.filter({ $0.date == date.string(format: "d/M/yy") })
        let datesSpending = datesTransactions.reduce(0) { $0 + $1.amount! }
        if date > today {
            return UIColor.clear
        } else {
            var green: CGFloat = 255
            var red: CGFloat = 255
            let percentageLeft = CGFloat((dailyBudget - datesSpending)/dailyBudget)
            if percentageLeft > 0.5 {
                red = red * (1 - percentageLeft)
            } else {
                green = green * percentageLeft
            }
            return UIColor(red: red/255, green: green/255, blue: 0.2, alpha: 1.0)
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
        return UIColor.blue
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        return UIColor.black
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        let today = Date()
        let datesTransactions = transactions.filter({ $0.date == date.string(format: "d/M/yy") })
        let datesSpending = datesTransactions.reduce(0) { $0 + $1.amount! }
        if date > today {
            return UIColor.clear
        } else {
            var green: CGFloat = 255
            var red: CGFloat = 255
            let percentageLeft = CGFloat((dailyBudget - datesSpending)/dailyBudget)
            if percentageLeft > 0.5 {
                red = red * (1 - percentageLeft)
            } else {
                green = green * percentageLeft
            }
            return UIColor(red: red/255, green: green/255, blue: 0.2, alpha: 1.0)
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        updateTransactionsData(date: date)
        selectedDate = date
        self.tableView.scrollToRow(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
        self.tableView.reloadData()
    }
}
