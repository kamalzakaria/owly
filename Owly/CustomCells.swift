//
//  CustomCells.swift
//  Owly
//
//  Created by Mohamad Kamal Zakaria on 06/10/2018.
//  Copyright © 2018 Nikz Tech. All rights reserved.
//

import UIKit
import FSCalendar
import SwiftGifOrigin

class SummaryCell: UITableViewCell {
    
    var pointsValue = MyLabel(text: "14000")
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        
        let titleLabel = UILabel()
        titleLabel.text = "Your Owly Score is"
        titleLabel.textColor = UIColor.white
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        titleLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
        
        pointsValue.text = "420"
        pointsValue.textColor = UIColor.white
        pointsValue.textAlignment = .center
        pointsValue.font = UIFont.systemFont(ofSize: 48, weight: .medium)
        
        let rankLabel = UILabel()
        rankLabel.text = "Master"
        rankLabel.textColor = UIColor.white
        rankLabel.textAlignment = .center
        rankLabel.font = UIFont.systemFont(ofSize: 19, weight: .regular)
        
        let textStack = UIStackView(arrangedSubviews: [titleLabel, pointsValue, rankLabel])
        textStack.setup(axis: .vertical, distribution: .fill, alignment: .fill, spacing: nil)
        textStack.setContentHuggingPriority(.required, for: .horizontal)
        
        let owlyImage = UIImageView()
        owlyImage.image = UIImage.gif(name: "owly-dashboard")
        owlyImage.contentMode = .scaleAspectFit
        owlyImage.setContentHuggingPriority(.required, for: .horizontal)
        
        let space = UIView()
        
        let stackView = UIStackView(arrangedSubviews: [space, textStack, owlyImage])
        stackView.setup(axis: .horizontal, distribution: .fill, alignment: .center, spacing: 10)
        
        addSubview(stackView)
        
        stackView.inflate(view: self, margin: 30)
        owlyImage.setConstraint(view: self, top: nil, leading: nil, bottom: nil, trailing: 50, width: nil, height: 120, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}




// ===== CALENDAR CELL =====

class CalendarTableViewCell: UITableViewCell, FSCalendarDelegate , FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    let calendarView = FSCalendar()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        
        calendarView.placeholderType = .none
        calendarView.delegate = self
        calendarView.dataSource = self
        calendarView.backgroundColor = UIColor.white
        calendarView.layer.cornerRadius = 10
        calendarView.clipsToBounds = true
        calendarView.dropShadow()
        calendarView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        addSubview(calendarView)
        
        calendarView.setConstraint(view: self, top: 0, leading: 20, bottom: 10, trailing: 20, width: nil, height: nil, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



// ===== TRANSACTION CELL =====

class TransactionCell: UITableViewCell {
    
    var transaction: Transaction? {
        didSet {
            name.text = transaction?.name
            amount.text = transaction?.amount?.currency
        }
    }
    
    let name = UILabel()
    let amount = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        clipsToBounds = true
        
        name.textColor = UIColor.gray
        name.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        
        amount.textAlignment = .right
        amount.textColor = UIColor.lightGray
        amount.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        
        let stackView = UIStackView(arrangedSubviews: [name, amount])
        stackView.setup(axis: .horizontal, distribution: .fill, alignment: .fill, spacing: nil)
        
        let cellView = UIView()
        cellView.backgroundColor = UIColor.white
        cellView.addSubview(stackView)
        cellView.dropShadow()
        
        addSubview(cellView)
        
        cellView.setConstraint(view: self, top: -5, leading: 20, bottom: -5, trailing: 20, width: nil, height: nil, x: nil, y: nil)
        stackView.setConstraint(view: cellView, top: 5, leading: 10, bottom: 5, trailing: 10, width: nil, height: nil, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// ===== TRANSACTION CELL 2.0 =====

class ExpenseCell: UITableViewCell {
    
    var selectedDate: Date? {
        didSet {
            dateTitle.text = selectedDate?.string(format: "d/M/yy")
        }
    }
    
    var todaysSpending: Float? {
        didSet {
            totalValue.text = todaysSpending?.currency
        }
    }
    
    let dateTitle = UILabel()
    let totalValue = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        clipsToBounds = true
        
        dateTitle.text = "31 OCT 2018"
        dateTitle.textColor = UIColor.gray
        dateTitle.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        
        let expenseText = UILabel()
        expenseText.text = "Expenses"
        expenseText.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        
        let dateStack = UIStackView(arrangedSubviews: [dateTitle, expenseText])
        dateStack.setup(axis: .vertical, distribution: .fill, alignment: .fill, spacing: nil)
        
        let totalTitle = UILabel()
        totalTitle.text = "TOTAL"
        totalTitle.textColor = UIColor.gray
        totalTitle.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        
        totalValue.text = "RM53.80"
        totalValue.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        
        let valueStack = UIStackView(arrangedSubviews: [totalTitle, totalValue])
        valueStack.setup(axis: .vertical, distribution: .fill, alignment: .fill, spacing: nil)
        valueStack.setContentHuggingPriority(.required, for: .horizontal)
        
        let stackView = UIStackView(arrangedSubviews: [dateStack, valueStack])
        stackView.setup(axis: .horizontal, distribution: .fill, alignment: .fill, spacing: nil)
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        headerView.addSubview(stackView)
        headerView.layer.cornerRadius = 10
        headerView.clipsToBounds = true
        headerView.dropShadow()
        
        addSubview(headerView)
        
        headerView.setConstraint(view: self, top: 20, leading: 20, bottom: -10, trailing: 20, width: nil, height: nil, x: nil, y: nil)
        stackView.setConstraint(view: headerView, top: 10, leading: 10, bottom: 20, trailing: 10, width: nil, height: nil, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BottomCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        clipsToBounds = true
        
        let bottomView = UIView()
        bottomView.backgroundColor = UIColor.white
        bottomView.layer.cornerRadius = 10
        bottomView.clipsToBounds = true
        bottomView.dropShadow()
        
        addSubview(bottomView)
        
        bottomView.setConstraint(view: self, top: -10, leading: 20, bottom: 30, trailing: 20, width: nil, height: 50, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class TransactionHeaderView: UITableViewHeaderFooterView {
    
    var title = UILabel()
    var totalTransaction = UILabel()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        clipsToBounds = true
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        self.backgroundView = backgroundView
        
        title.font = UIFont.systemFont(ofSize: 19, weight: .medium)
        title.textColor = UIColor.black
        
        totalTransaction.font = UIFont.systemFont(ofSize: 19, weight: .medium)
        totalTransaction.textColor = UIColor.black
        totalTransaction.textAlignment = .right
        
        let line = UIView()
        line.backgroundColor = UIColor.gray
        
        let stackView = UIStackView(arrangedSubviews: [title, totalTransaction])
        stackView.setup(axis: .horizontal, distribution: .fill, alignment: .fill, spacing: nil)
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        headerView.addSubview(stackView)
        headerView.dropShadow()
        
        backgroundView.addSubview(headerView)
        headerView.addSubview(line)
        
        headerView.setConstraint(view: backgroundView, top: -5, leading: 20, bottom: -5, trailing: 20, width: nil, height: nil, x: nil, y: nil, safeAreaLayout: false)
        stackView.setConstraint(view: headerView, top: 5, leading: 10, bottom: 5, trailing: 10, width: nil, height: nil, x: nil, y: nil)
        line.setConstraint(view: headerView, top: nil, leading: 10, bottom: 7, trailing: 10, width: nil, height: 1, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

