//
//  SettingsViewController.swift
//  Owly
//
//  Created by Mohamad Kamal Zakaria on 06/10/2018.
//  Copyright © 2018 Nikz Tech. All rights reserved.
//

import UIKit
import SwiftGifOrigin

protocol SettingsDelegate {
    func didSave(budget: Float, income: Float, houseLoan: Float, carLoan: Float, personalLoan: Float)
}

class SettingsViewController: UIViewController {
    
    var cells = [UITableViewCell]()
    let tableView = UITableView()
    let monthlyCell = MonthlyIncomeView()
    let expensesCell = MonthlyExpensesView()
    var stackView: UIStackView!
    
    var delegate: SettingsDelegate?
    
    var income: Float = 3000
    var houseLoan: Float = 0
    var carLoan: Float = 0
    var personalLoan: Float = 0
    var budget: Float = 3000 {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        setupViews()
        setupParam()
    }
    
}

extension SettingsViewController {
    func setupParam() {
        monthlyCell.incomeField.inputField.text = income.clean
        expensesCell.houseLoanField.inputField.text = houseLoan == 0 ? "" : houseLoan.clean
        expensesCell.carLoanField.inputField.text = carLoan == 0 ? "" : carLoan.clean
        expensesCell.personalLoanField.inputField.text = personalLoan == 0 ? "" : personalLoan.clean
    }
    
    func setupViews() {
        
        view.backgroundColor = UIColor.white
        
        let closeButton = UIButton(type: .custom)
        closeButton.setImage(#imageLiteral(resourceName: "Cancel"), for: .normal)
        closeButton.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        
        let bgImage = UIImageView()
        bgImage.contentMode = .scaleAspectFill
        bgImage.image = #imageLiteral(resourceName: "background")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        
        let headerCell = SettingHeaderCell()
        
        monthlyCell.incomeField.inputField.delegate = self
        monthlyCell.incomeField.inputField.tag = 0
        
        expensesCell.carLoanField.inputField.delegate = self
        expensesCell.houseLoanField.inputField.delegate = self
        expensesCell.personalLoanField.inputField.delegate = self
        expensesCell.houseLoanField.inputField.tag = 1
        expensesCell.carLoanField.inputField.tag = 2
        expensesCell.personalLoanField.inputField.tag = 3
        
        let summaryCell = SettingsSummaryCell()
        
        cells = [headerCell, monthlyCell, expensesCell, summaryCell]
        
        let saveButton = UIButton(type: .system)
        let attributedString = NSAttributedString(string: "Save", attributes: [.font: UIFont.systemFont(ofSize: 24, weight: .medium), .foregroundColor: UIColor.white])
        saveButton.setAttributedTitle(attributedString, for: .normal)
        saveButton.addTarget(self, action: #selector(saveTapped), for: .touchUpInside)
        saveButton.setBackgroundImage(#imageLiteral(resourceName: "save-background"), for: .normal)
        
        stackView = UIStackView(arrangedSubviews: [tableView, saveButton])
        stackView.setup(axis: .vertical, distribution: .fill, alignment: .fill, spacing: 20)
        
        view.addSubview(bgImage)
        view.addSubview(stackView)
        view.addSubview(closeButton)
        
        stackView.inflate(view: view, margin: 0)
        bgImage.setConstraint(view: view, top: 0, leading: 0, bottom: nil, trailing: 0, width: nil, height: nil, x: nil, y: nil, safeAreaLayout: false)
        closeButton.setConstraint(view: view, top: 10, leading: nil, bottom: nil, trailing: 10, width: nil, height: nil, x: nil, y: nil)
    }
}


extension SettingsViewController {
    @objc func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func saveTapped() {
        delegate?.didSave(budget: budget / 30, income: income, houseLoan: houseLoan, carLoan: carLoan, personalLoan: personalLoan)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        
        stackView.spacing = keyboardFrame.height + 20
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        stackView.spacing = 20
    }
}


extension SettingsViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let amount = textField.text?.float else { return }
        switch textField.tag {
        case 0:
            self.income = amount
            self.budget = income - houseLoan - carLoan - personalLoan
        case 1:
            self.houseLoan = amount
            self.budget = income - houseLoan - carLoan - personalLoan
        case 2:
            self.carLoan = amount
            self.budget = income - houseLoan - carLoan - personalLoan
        case 3:
            self.personalLoan = amount
            self.budget = income - houseLoan - carLoan - personalLoan
        default:
            print("")
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.selectAllText()
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 3:
            let cell = cells[indexPath.row] as! SettingsSummaryCell
            cell.monthlyBudget = budget
        default:
            print("")
        }
        return cells[indexPath.row]
    }
}


class MonthlyIncomeView: UITableViewCell {
    
    let incomeField = CustomTextField()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        
        let container = UIView()
        container.backgroundColor = UIColor.white
        container.layer.cornerRadius = 10
        container.clipsToBounds = true
        container.dropShadow()
        
        let titleLabel = UILabel()
        titleLabel.text = "What is your monthly income?"
        titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        
        incomeField.inputField.text = "3000"
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, incomeField])
        stackView.setup(axis: .vertical, distribution: .fill, alignment: .fill, spacing: 10)
        
        container.addSubview(stackView)
        addSubview(container)
        
        container.inflate(view: self, margin: 20)
        stackView.setConstraint(view: container, top: 10, leading: 20, bottom: 40, trailing: 20, width: nil, height: nil, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class MonthlyExpensesView: UITableViewCell {
    
    let houseLoanField = CustomTextField()
    let carLoanField = CustomTextField()
    let personalLoanField = CustomTextField()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        
        let container = UIView()
        container.backgroundColor = UIColor.white
        container.layer.cornerRadius = 10
        container.clipsToBounds = true
        container.dropShadow()
        
        let titleLabel = UILabel()
        titleLabel.text = "How much is your monthly expenses?"
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        
        houseLoanField.inputField.placeholder = "Housing Loan"

        carLoanField.inputField.placeholder = "Car Loan"

        personalLoanField.inputField.placeholder = "Personal Loan"
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, houseLoanField, carLoanField, personalLoanField])
        stackView.setup(axis: .vertical, distribution: .fill, alignment: .fill, spacing: 20)
        
        container.addSubview(stackView)
        addSubview(container)
        
        stackView.setConstraint(view: container, top: 10, leading: 20, bottom: 30, trailing: 20, width: nil, height: nil, x: nil, y: nil)
        container.setConstraint(view: self, top: 10, leading: 20, bottom: 20, trailing: 20, width: nil, height: nil, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class SettingHeaderCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        
        let owlyImageView = UIImageView()
        owlyImageView.image = UIImage.gif(name: "owly-dashboard")
        owlyImageView.contentMode = .scaleAspectFit
        owlyImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        owlyImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        let titleLabel = UILabel()
        titleLabel.text = "Tell us about your financial background"
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.systemFont(ofSize: 26, weight: .medium)
        titleLabel.textColor = UIColor.white
        
        let titleStack = UIStackView(arrangedSubviews: [owlyImageView, titleLabel])
        titleStack.setup(axis: .horizontal, distribution: .fill, alignment: .fill, spacing: 20)
        
        addSubview(titleStack)
        
        titleStack.setConstraint(view: self, top: 0, leading: 20, bottom: 0, trailing: 20, width: nil, height: nil, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class SettingsSummaryCell: UITableViewCell {
    
    var monthlyBudget: Float? {
        didSet {
            guard let monthlyBudget = monthlyBudget else { return }
            let dailyBudget = monthlyBudget / 30
            summaryLabel.text = "You have \(monthlyBudget.currency) to spend monthly"
            dailyBudgetLabel.text = "Your daily spending is \(dailyBudget.currency)"
        }
    }
    
    let summaryLabel = UILabel()
    let dailyBudgetLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        summaryLabel.numberOfLines = 0
        
        dailyBudgetLabel.numberOfLines = 0
        
        let stackView = UIStackView(arrangedSubviews: [summaryLabel, dailyBudgetLabel])
        stackView.setup(axis: .vertical, distribution: .fill, alignment: .fill, spacing: 10)
        
        addSubview(stackView)
        
        stackView.setConstraint(view: self, top: 0, leading: 20, bottom: 20, trailing: 20, width: nil, height: nil, x: nil, y: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
