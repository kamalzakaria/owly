//
//  TabBarController.swift
//  Owly
//
//  Created by Mohamad Kamal Zakaria on 07/10/2018.
//  Copyright © 2018 Nikz Tech. All rights reserved.
//

import UIKit

class TabbarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let mainView = ViewController()
        let mainbaritem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "Home-inactive"), tag: 0)
        mainView.tabBarItem = mainbaritem
        
        let chartsView = ImageViewController()
        chartsView.image = #imageLiteral(resourceName: "Track")
        let chartitem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "Track-inactive"), tag: 1)
        chartsView.tabBarItem = chartitem
        
        let leaderBoardView = ImageViewController()
        let leaderBoarditem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "Leaderboard-active"), tag: 2)
        leaderBoardView.tabBarItem = leaderBoarditem
        leaderBoardView.image = #imageLiteral(resourceName: "Leaderboards")
        
        viewControllers = [mainView, chartsView,leaderBoardView]
    }
}



class ImageViewController: UIViewController {
    
    var image: UIImage? {
        didSet {
            guard let image = image else { return }
            imageView.image = image
        }
    }
    
    
    let imageView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        imageView.contentMode = .scaleAspectFill
        
        view.addSubview(imageView)
        
        
        imageView.setConstraint(view: view, top: 0, leading: 0, bottom: 0, trailing: 0, width: nil, height: nil, x: nil, y: nil, safeAreaLayout:  false)
    }
}
