//
//  extensions.swift
//  Owly
//
//  Created by Mohamad Kamal Zakaria on 06/10/2018.
//  Copyright © 2018 Nikz Tech. All rights reserved.
//

import UIKit

extension UIView {
    
    func inflate(view: UIView, margin: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: margin),
            self.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: margin),
            self.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -margin),
            self.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -margin),
            ])
    }
    
    func setConstraint(view: UIView, top: CGFloat?, leading: CGFloat?, bottom: CGFloat?, trailing: CGFloat?, width: CGFloat?, height: CGFloat?, x: CGFloat?, y: CGFloat?, safeAreaLayout: Bool? = true) {
        translatesAutoresizingMaskIntoConstraints =  false
        if safeAreaLayout == false {
            if let top = top { topAnchor.constraint(equalTo: view.topAnchor, constant: top).isActive = true }
            if let leading = leading { leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leading).isActive = true }
            if let bottom = bottom { bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -bottom).isActive = true }
            if let trailing = trailing { trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -trailing).isActive = true }
        } else {
            if let top = top { topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: top).isActive = true }
            if let leading = leading { leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: leading).isActive = true }
            if let bottom = bottom { bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -bottom).isActive = true }
            if let trailing = trailing { trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -trailing).isActive = true }
        }
        if let width = width {
            if width < 1 && width > 0 { widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: width).isActive = true
            } else { widthAnchor.constraint(equalToConstant: width).isActive = true }}
        if let height = height {
            if height < 1 && height > 0 { heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: height)
            } else { heightAnchor.constraint(equalToConstant: height).isActive = true }}
        if let x = x { centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: x).isActive = true }
        if let y = y { centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: y).isActive = true }
    }
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowRadius = 1
        
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}


extension UIStackView {
    
    func setup(axis: NSLayoutConstraint.Axis, distribution: UIStackView.Distribution, alignment: UIStackView.Alignment, spacing: CGFloat?) {
        self.axis = axis
        self.distribution = distribution
        self.alignment = alignment
        if let spacing = spacing { self.spacing = spacing }
    }
    
}


extension Sequence {
    func group<GroupingType: Hashable>(by key: (Iterator.Element) -> GroupingType) -> [[Iterator.Element]] {
        var groups: [GroupingType: [Iterator.Element]] = [:]
        var groupsOrder: [GroupingType] = []
        forEach { element in
            let key = key(element)
            if case nil = groups[key]?.append(element) {
                groups[key] = [element]
                groupsOrder.append(key)
            }
        }
        return groupsOrder.map { groups[$0]! }
    }
}


extension Float {
    var currency: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 2
        let numberString = formatter.string(for: self)
        return numberString!
    }
    
    func localCurrenncy(locale: Locale) -> String {
        let formatter = NumberFormatter()
        formatter.locale = locale
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 2
        let numberString = formatter.string(for: self)
        return numberString!
    }
    
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}




extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}



extension String {
    var isNumeric: Bool {
        guard self.characters.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self.characters).isSubset(of: nums)
    }
    
    func date(_ format: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: self)!
    }
    
    var float: Float {
        let filteredString = String(self.filter { "0123456789.".contains($0) })
        return filteredString == "" ? 0 : Float(filteredString)!
    }
}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension UITextField {
    func selectAllText() {
        selectedTextRange = textRange(from: beginningOfDocument, to: endOfDocument)
    }
}
